from opcua import Client
import sys
import time

url='<<OPC_UA_TCP_ENDPOINT_OBTAINED_FROM_ENDPOINTS_TAB(not the status tab)>>'

try:
    client=Client(url)
    client.connect()
    print("Client Connected")
except Exception as err:
    print("Error while creating connection",err)
    sys.exit(1)

if __name__ == '__main__':
    node1 = client.get_node("ns=3;i=1003")
    while True:
        node1_value=node1.get_value()
        print(node1_value)
        time.sleep(2)